problem: |-
  Alexander is juggling balls. At the start, he has four balls in his left hand: a red ball, a blue ball, a yellow ball, and a green ball. Each second, he tosses a ball from one hand to the other. If Alexander juggles for $20$ seconds and the number of ways for Alexander to toss the balls such that he has all four balls in his right hand at the end is $N$, find the number of positive divisors of $N$.
author: Dennis Chen
answer: 640
solutions:
  - text: |-
      Let $L$ and $R$ denote the number of ways to finish with all of the balls in his left and right hand, respectively. Then the idea is trying to find $L+R$ and $L-R$.
  
      Note that $L+R$ is just the number of ways to end with all four balls in one hand. We use a perspectives-style\footnote{``To be more specific, this is part of the \emph{Freedom} section of my Perspectives handout.'' --- Dennis} argument: at the end of $18$ seconds, he either has $4$ balls all in the same hand or $2$ balls in each. Now notice that no matter what, at the $19$th second, he will always have $3$ balls in one hand and $1$ in the other. Now at the last second, there is exactly one move that can get him to four balls in one hand, so $L+R=4^{19}$.
  
      Now we examine $L-R$. Note that if Alexander ever has two balls in the same hand, then the number of ways to finish with all balls in the left hand is the same as finishing with the right hand by symmetry. Thus, the only situation that contributes to $L-R$ is when we continually alternate between $4$ balls in the left hand, none in the right and $3$ balls in the left hand, $1$ on the right! Note that to have all balls in the right hand, we must have $2$ balls in the right hand at some point, so we just need to count the number of ways to end with all $4$ balls in the left hand without two ever reaching the right hand at the same time. The total difference is just $L-R=4^{10}$.
  
      Now note that
      \[R=\frac{(L+R)-(L-R)}{2}=\frac{2^{38}-2^{20}}{2}=2^{37}-2^{19}=2^{19}(2^{18}-1),\]
      and by factoring with difference of squares and sums/differences of cubes, we get that this is equivalent to
      \[2^{19} \cdot 3^3 \cdot 7 \cdot 19 \cdot 73.\]
      Thus the answer is $20\cdot 4\cdot 2\cdot 2\cdot 2=\ansbold{640}$.
    author: Aaron Guo
  - text: |-
      Let $L_n, C_n$, and $R_n$ be the number of ways to finish with all balls in left, two balls in left, and zero balls in left, respectively after $2n$ seconds.
      We have
      \[L_n=4L_{n-1}+2C_{n-1}\]
      \[C_n=12C_{n-1}+12L_{n-1}+12R_{n-1}\]
      \[R_n=2C_{n-1}+4R_{n-1},\]
      so because $L_n +C_n + R_n = 4^{2n}$ ($4^{2n}$ is the total for choosing one of the four balls to move every time), we see that
      \[C_n=3 \cdot 4^{2n-1}.\]
      Note that $R_0=R_1=0$, and we have the recursion
      \[R_n = 3 \cdot 2^{4n-1} + 4R_{n-1}, \]
      so $R_2 =3 \cdot 2^3 =24$ and $R_{n-1} = 3 \cdot 2^{4n-5} +4R_{n-2}$.
      This allows us to cancel the external powers of $2$ and get
      \[R_n = 20R_{n-1} -64R_{n-2}. \]
      Thus, we have the characteristic polynomial $c^2 - 20c + 64=0$, so $c=4,16$ and
      \[R_n = a \cdot 4^n + b \cdot 16^n . \]
      We have $4a+16b=0$ and $16a+256b=24$, so $a= -\frac{1}{2}$ and $b=\frac{1}{8}$.
      $N$ is
      \[ R_{10} = -\frac{1}{2} \cdot 4^{10} + \frac{1}{8} \cdot 16^{10} = 2^{19}(2^{18} -1) \]
      and by factoring with difference of squares and sums/differences of cubes, we get that this is equivalent to
      \[2^{19} \cdot 3^3 \cdot 7 \cdot 19 \cdot 73.\]
      Thus the answer is $20\cdot 4\cdot 2\cdot 2\cdot 2=\ansbold{640}$.
    author: Aaron Guo
  - text: |-
      We use generating functions. What we want is clearly the sum of the terms in $P(a,b,c,d) = (a+b+c+d)^{20}$ with all odd exponents (where at each turn we have 4 options for each of the four balls, and we want the number of swaps to be odd).\\
      Let the answer be $A$, and let $S$ be the set of 4-tuples $s = (s(1), s(2), s(3), s(4))$ such that $s(i)\in{-1,1}$.
  
      The central claim is that
      \[16A = \sum_{s\in S} P(s)\prod_{i = 1}^4(s(i))\text{.}\]
  
      \textbf{Proof}: Let
      \[Q(a,b,c,d) = \binom{20}{w,x,y,z}a^wb^xc^yd^z\]
      be a term of $P(a,b,c,d)$. If any of $w,x,y,z$ are even, then, without loss of generality, assume it's $w$. Then
      \[Q(1,x,y,z) - Q(-1,x,y,z) = 0\]
      so we can pair up the tuples $(-1,s(2),s(3),s(4)), (1,s(2),s(3),s(4))$ to show that
      \[\sum_{s\in S} Q(s)\prod_{i = 1}^4(s(i))\]
      vanishes. However, if they are all odd, then they are counted positively $16$ times.
  
      Thus, the answer is
      \[\frac{\sum_{s\in S} P(s)\prod_{i = 1}^4(s(i))}{16} = \frac{2^{40} - 4(2^{20}) - 4(2^{20}) + 2^{40}}{16} = 2^{37} - 2^{19} = 2^{19} \cdot 3^3 \cdot 7 \cdot 19 \cdot 73\]
      giving us an answer of $\ansbold{640}$.
  
      Note that this solution is the only one that doesn't take advantage of $4$ being small, and in fact extends to basically any $n$.
    author: Ethan Liu
