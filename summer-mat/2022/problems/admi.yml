problem: Let $\triangle ABC$ be a scalene triangle with incenter $I$, and let the midpoint of side $\overline{BC}$ be $M$. Suppose that the feet of the altitudes from $I$ to $\overline{AC}$ and $\overline{AB}$ are $E$ and $F$ respectively, and suppose that there exists a point $D$ on $\overline{EF}$ such that $ADMI$ is a parallelogram. If $AB+AC = 24$, $BC = 14$, and the area of $\triangle ABC$ can be expressed as $m\sqrt{n}$ where $m$ and $n$ are positive integers such that $n$ is squarefree, find $m+n$.
author: Aprameya Tripathy
answer: 29
solutions:
  - text: |-
      Let $\overline{BI}$ and $\overline{CI}$ intersect $\overline{EF}$ at $P$ and $Q$ respectively. It is well known (by the Iran Lemma) that $P$ and $Q$ lie on the circle centered at $M$ with radius $MB=MC$. Since $\overline{MD} \parallel \overline{AI} \perp \overline{PQ}$, we also know that $\angle MDP = 90^{\circ}$.

      Now, notice that
      \[\angle DMP = \frac{1}{2} \angle QMP = \angle QBP = \angle BIC - 90^{\circ} = \angle EAI.\]
      Therefore, $\triangle MDP \sim \triangle AEI$, so
      \[\frac{AI}{7} = \frac{AI}{MB} = \frac{MD}{MP} = \frac{AF}{AI} = \frac{\frac{AB+AC-BC}{2}}{AI} = \frac{5}{AI}.\]
      Thus, $AI = \sqrt{35}$, so by the Pythagorean Theorem on $\triangle AIE$, the inradius of $\triangle ABC$ is $\sqrt{10}$.

      Since $\triangle ABC$ has semiperimeter $19$, the area of $\triangle ABC$ is $19\sqrt{10}$ by $K=rs$, so the answer is $19 + 10 = \ansbold{29}$.
    author: Aprameya Tripathy
  - text: |-
      In this solution, we denote the distance from point $P$ to line $\ell$ as $\delta(P, \ell)$.

      Note that $AI\perp EF$, so $MD\perp EF$ as well. This means we want $AI=\delta(M, EF)$.

      Note that $\delta(M, EF)=\frac{\delta(B, EF) + \delta(C, EF)}{2}$. Let the foot of $B$ to $EF$ be $P$, and note that $\angle FPB=\angle AFE=90^{\circ}-\frac{\angle A}{2}$, so $BP=BF\cos\frac{A}{2}$. Similarly, if we let $Q$ be the foot from $C$ to $EF$, $CQ=CE\cos\frac{\angle A}{2}$. Thus
      \[\frac{\delta(B, EF) + \delta(C, EF)}{2} = \frac{BP + CQ}{2} = \frac{(BF+CE)\cos\frac{A}{2}}{2} = \frac{BC\cos\frac{\angle A}{2}}{2} = 7\cos\frac{\angle A}{2}.\]

      \begin{center}
        \begin{asy}
        import geometry;
        size(8cm);
        pair A = (2, 7);
        pair B = (0, 0);
        pair C = (14, 0);

        pair I = incenter(A, B, C);
        pair E = projection(A, C)*I;
        pair F = projection(A, B)*I;

        pair P = projection(E, F)*B;
        pair Q = projection(E, F)*C;

        dot("$A$", A, N);
        dot("$B$", B, SW);
        dot("$C$", C, SE);
        dot("$E$", E, N);
        dot("$F$", F, NW);
        dot("$P$", P, NW);
        dot("$Q$", Q, N);

        draw(A--B--C--cycle);
        draw(B--P--Q--C);

        perpendicular(P, SE, F-P);
        perpendicular(Q, NE, E-Q);
        \end{asy}
      \end{center}

      Since $ADMI$ is a parallelogram, $AI=DM$. Note that $AI=\frac{AE}{\cos\frac{A}{2}}$, and since $AE=s-a=5$, $AI=\frac{5}{\cos\frac{\angle A}{2}}$. Since $DM=7\cos\frac{\angle A}{2}$, $AI=DM$ implies $\cos\frac{\angle A}{2}=\sqrt{\frac{5}{7}}$.

      Thus $\cos A = 2\cos^2\frac{\angle A}{2} - 1 = \frac{3}{7}$. This implies $\sin A = \frac{2\sqrt{10}}{7}$, which we will use later.

      By the Law of Cosines,
      \[b^2-\frac{6}{7}bc+c^2=14^2,\]
      and we are given that $b+c=24$. Squaring gives
      \[b^2+2bc+c^2=24^2,\]
      and subtracting the first equation from the second gives $bc=133$.

      Now note the area of $\triangle ABC$ is equal to
      \[\frac{1}{2}bc\sin\angle A = \frac{1}{2} \cdot 133 \cdot \frac{2\sqrt{10}}{7} = 19\sqrt{10}.\]
      Thus our answer is $19 + 10 = \ansbold{29}$.
    author: Dennis Chen
  - text: |-
      We use normalized barycentric coordinates. Let $a = BC$, $b = CA$, $c = AB$, and $s = \frac{a + b + c}{2}$. Let $A = (1, 0, 0)$, $B = (0, 1, 0)$, and $C = (0, 0, 1)$. Then, $M = (0, \frac{1}{2}, \frac{1}{2})$, and $I = (\frac{7}{19}, \frac{b}{38}, \frac{24 - b}{38})$. Note that $\overrightarrow{AI} = (\frac{12}{19}, -\frac{b}{38}, \frac{b - 24}{38})$, so
      \[D = M + \overrightarrow{AI} =  \left(\frac{12}{19}, \frac{19 - b}{38}, \frac{b - 5}{38}\right).\]
      It is well-known that $\frac{AF}{FB} = \frac{s - a}{s - b} = \frac{5}{19 - b}$ and $\frac{AE}{EC} = \frac{s - a}{s - c} = \frac{5}{19 - c} = \frac{5}{b - 5}$. Then, $E = (\frac{b - 5}{b}, 0, \frac{5}{b})$, and $F = (\frac{19 - b}{24 - b}, \frac{5}{24 - b}, 0)$. Since $D$, $E$, and $F$ are collinear,
      \begin{align*}
        0 &= \begin{vmatrix}
               1 & \frac{5}{24 - b} & 0 \\ 1 & 0 & \frac{5}{b} \\
               1 & \frac{19 - b}{38} & \frac{b - 5}{38}
             \end{vmatrix} \\
          &= \frac{b - 19}{38} \cdot \frac{5}{b} - \frac{5}{24 - b} \left(\frac{b - 5}{38} - \frac{5}{b}\right) \\
          &= \frac{5((b - 19)(24 - b) - b(b - 5) + 190)}{38b(24 - b)} \\
          &= -\frac{5(b^2 - 24b + 133)}{19b(24 - b)}.
      \end{align*}
      Solving $b^2 - 24b + 133 = 0$ yields $b = 12 \pm \sqrt{11}$. Letting $b = 12 + \sqrt{11}$, Heron's formula gives
      \[[ABC] = \sqrt{19(5)(7 + \sqrt{11})(7 - \sqrt{11})} = 19\sqrt{10},\]
      so the answer is $19 + 10 = \ansbold{29}$.
    author: Prajith Velicheti
