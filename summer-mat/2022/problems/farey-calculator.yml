## buckets
good: true
mat: true

problem: |-
  Farey's calculator always displays exactly $10$ digits after the decimal point, and has one operation: $\sqrt{x}.$ Pressing the button will output the square root of the input, with everything beyond the $10$ digits erased (these erased digits are now forgotten by the calculator). Farey enters
  \[ 1.000000\underline{abcd}\]
  where $a$, $b$, $c$, and $d$ are digits, and continues pressing the $\sqrt{x}$ operator until the calculator displays $1.0000000000$. Find the unique four-digit number $\underline{abcd}$ such that the number of times the rightmost digit displayed is even is maximized.
author: Aaron Guo
answer: 8190
solutions:
  - text: |-
      Define a function $f$ such that the result of pressing the $\sqrt{}$ operator on $x$ is $f(x)$. For positive integers $k<10^5$, we claim that $f(1+10^{-10}k)=1+10^{-10}\lfloor\frac{k-1}{2}\rfloor$. The motivation for making this claim is that for sufficiently small $\epsilon$, $\sqrt{1+\epsilon}\approx \sqrt{1+\epsilon+\frac{\epsilon^2}{4}} = 1+\frac{\epsilon}{2}$.

      To prove this, we need to show
      \[1+10^{-10}\lfloor\frac{k-1}{2}\rfloor < \sqrt{1+10^{-10}k} < 1+10^{-10}(\lfloor\frac{k-1}{2}+1)\]
      for $1<k<10^5$. Since all values involved are positive, it suffices to square everything and show
      \[(1+10^{-10}\lfloor\frac{k-1}{2}\rfloor)^2 < 1+10^{-10}k < (1+10^{-10}(\lfloor\frac{k-1}{2}\rfloor+1))^2.\]
      But note that
      \[(1+10^{-10}\lfloor\frac{k-1}{2}\rfloor)^2 \leq (1+10^{-10}\frac{k-1}{2})^2 = 1+10^{-10}(k-1)+(10^{-10}\frac{k-1}{2})^2 < 1+10^{-10}k,\]
      the final inequality of the chain being true as $(10^{-10}\frac{k-1}{2})^2 < (10^{-10}\cdot\frac{10^5}{2})^2 < 10^{-10}$.
      Similarly, note that
      \[(1+10^{-10}(\lfloor\frac{k-1}{2}\rfloor+1))^2 \geq (1+10^{-10}\frac{k}{2})^2 > 1+10^{-10}k.\]
      We have shown that $f(1+10^{-10}k) = 1+10^{-10}\lfloor\frac{k-1}{2}\rfloor$, as desired.

      Now we work backwards in order to construct a sequence of displayed numbers $a_n$, where $a_{n+1}$ is what was displayed \emph{before} $a_n$, i.e. the calculator displayed $a_n$ \emph{after} the $\sqrt{}$ function was applied to $a_{n+1}$. Note that the process ends when the rightmost four digits displayed on the calculator are $0000$, so define $a_0=0$. Note that by definition, we can either have $a_{n+1}=2a_n+1$ or $2a_n+2$ for each $n$. Also note that the sequence $a_n$ stops when it is impossible for the next number in the sequence to be less than $10^5$.

      We claim the largest number of even numbers that can appear in the sequence $a_n$ is $13$, where $a_0=0$ and $a_{n+1}=2a_n+2$ for $n\geq 0$. For $n\geq 1$, $a_n=\sum\limits_{i=1}^n 2^n$, implying $a_{12}=8190$. Furthermore, we claim that the described sequence \emph{uniquely} produces this maximum.

      Since the number of even integers in our sequence is the number of times $a_{n+1}=2a_n+2$ is true (plus one), we claim that the only way to have $a_{n+1}=2a_n+2$ occur $12$ times is for $a_n=2a_n+1$ to occur zero times for size reasons. Note that if such a sequence has $a_{n+1}=2a_n+1$ occur more than once, we may remove all but the first occurrence and have a smaller maximum in the sequence.\footnote{Proof left as exercise since it's fairly straightforward, the result is intuitively true, and the solution is long enough as is.} For example, $0\to 1\to 3\to 8$ has two occurrences of $a_{n+1}+1$ while $0\to 1\to 4$ only has one occurrence of $a_{n+1}=2a_n+1$, since we have removed the second occurrence. Both sequences have the same number of occurrences of $a_{n+1}=2a_n+2$. Thus, we would like to show that any sequence with $a_{n+1}=2a_n+1$ occur once and $a_{n+1}=2a_n+2$ occur twelve times is bound to have $a_{13}\geq 10^5$.

      Note the minimal possible value of $a_3$ is $10$ and $a_{n+3}\geq 2^na_3$ as $a_{n}\geq 2a_{n-1}$.\footnote{This can be shown via induction.} Thus $a_{13}\geq 2^{10}\cdot 10=10240\geq 10^5$, as desired.

      Thus the only sequence with $13$ even numbers or more has $a_{12}=\ansbold{8190}$, which is our answer.
    author: Dennis Chen
