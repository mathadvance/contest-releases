problem: |-
  The mad scientist Kyouma is traveling on a number line from $1$ to $2020$, subject to the following rules:
    \begin{itemize}
    \item He starts at $1$.
    \item Each move, he randomly and uniformly picks a number greater than his current number to go to.
    \item If he reaches $2020$, he is instantly teleported back to $1$.
    \item There is a time machine on $199$.
    \item A foreign government is waiting to ambush him on $1729$.
    \end{itemize}
    What is the probability that he gets to the time machine before being ambushed?
author: Dennis Chen
answer: \frac{292}{2113}
solutions:
  - text: |-
      Say Kyouma is in state $A$ if he is between $1$ and $198$ inclusive or between $1730$ and $2020$ inclusive, and say he is in state $B$ if he is between $199$ and $1729$ inclusive. We note that the only way Kyouma can win or lose is by going from state $A$ to state $B$. Let $P(A)$ be the probability that he wins while in state $A$, and let $P(B)$ be the probability that he wins while in state $B$ \textbf{while the game is still going}. Note that
      \begin{align*}
      P(A)&=\frac{1}{1729-199+1}+\frac{1729-199-1}{1729-199+1}P(B), \text{ and} \\
      P(B)&=(1-\frac{1}{2020-1729+1})P(A),
      \end{align*}
      since we only care about the moves from $A$ to $B$, and vice versa.

      Now we just need to solve this equation. Note that this is equivalent to
      \begin{align*}
      P(A)&=\frac{1}{1531}+\frac{1529}{1531}P(B), \text{ and} \\
      P(B)&=\frac{291}{292}P(A),
      \end{align*}
      which implies $1531P(A)=1+1529\cdot \frac{291}{292}P(A)$, or
      \[(2+1529\cdot \frac{1}{292})P(A)=\frac{2113}{292}P(A)=1,\]
      so the answer is $\ansbold{\frac{292}{2113}}$ as we are looking for $P(A)$.
    author: Dennis Chen
