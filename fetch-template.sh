if [[ $# -ne 1 ]]; then
	echo Usage: ./fetch-template.sh SERIES
	# We use exitcode 64 as according to
	# https://www.freebsd.org/cgi/man.cgi?query=sysexits&apropos=0&sektion=0&manpath=FreeBSD+4.3-RELEASE&format=html
	exit 64
fi
# Just for convenience, so I remember stuff/can edit this script conveniently if needed
CONTEST=$1

TEMPLATE_URL=$(cat $CONTEST/template | sed -n 1p)
CLASS=$(cat $CONTEST/template | sed -n 2p)
TEMPLATE=${TEMPLATE_URL##*/}
CLASS_DIR=$(kpsewhich --var-value TEXMFHOME)/tex/latex/$TEMPLATE
TEMPLATE_DIR=$HOME/.config/mapm/templates/$TEMPLATE 
if [[ $CLASS = "class" ]]; then
	if [[ ! -d $CLASS_DIR ]]; then
		git clone $TEMPLATE_URL $CLASS_DIR
	fi
	if [[ ! -d $TEMPLATE_DIR ]]; then
		ln -s $CLASS_DIR/mapm $TEMPLATE_DIR
	fi
elif [[ $CLASS = "noclass" ]]; then
	if [[ ! -d $TEMPLATE_DIR ]]; then
		git clone $TEMPLATE_URL $TEMPLATE_DIR
	fi
else
	echo The second line of the file that specifies the template
	echo is neither "class" nor "noclass".
	echo Something has gone wrong.
	echo "(Are you sure you're in the right working directory?)"
	exit 64
fi
